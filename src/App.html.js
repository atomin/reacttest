import React from 'react';
import logo from './logo.svg';

function html() {
  return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div className="dropdown open">
          <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
          </button>
          <div style={{width: '300px'}} className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <ul>
              <li>
                first 1
                <ul>
                  <li>
                    second 1
                    <ul>
                      <li>
                        third 1
                      </li>
                      <li>
                        third 2
                      </li>
                    
                    </ul>
                  </li>
                  <li>
                    second 2
                    <ul>
                      <li>
                        third 5
                      </li>
                      <li>
                        third 6
                      </li>
                    
                    </ul>
                  </li>
                  <li>
                    second 3
                  </li>
                  <li>
                    second 4
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div> some test content under drop down </div>
      </div>

      
    );
}

export default html;

